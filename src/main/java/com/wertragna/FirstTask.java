package com.wertragna;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class FirstTask {
    private static final int MAX_CHAR = 26;
    private static final String STRINGS_ARE_NOT_ANAGRAMS = "Strings are not anagrams";
    private static final String STRINGS_ARE_ANAGRAMS = "Strings are anagrams";
    private static final String STRING_1 = "String1:";
    private static final String STRING_2 = "String2:";
    private static final String INPUT = "Input:";
    private static final String OUTPUT = "Output:\n\"";


    public static void main(String[] args) throws IOException {
        System.out.println(INPUT);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print(STRING_1);
        String firstString = br.readLine();
        System.out.print(STRING_2);
        String secondString = br.readLine();
        String result = verifyAnagrams(firstString, secondString);
        System.out.println(OUTPUT + result + "\"");
    }

    public static String verifyAnagrams(String firstString, String secondSting) {
        if (firstString.length() != secondSting.length())
            return STRINGS_ARE_NOT_ANAGRAMS;
        String firstStingLowerCase = firstString.toLowerCase();
        String secondStingLowerCase = secondSting.toLowerCase();
        int[] differenceBetweenStrings = new int[MAX_CHAR];
        for (int i = 0; i < firstString.length(); i++) {
            differenceBetweenStrings[firstStingLowerCase.charAt(i) - 'a']++;
            differenceBetweenStrings[secondStingLowerCase.charAt(i) - 'a']--;
        }
        List<Character> characters = new ArrayList<>();
        for (int i = 0; i < differenceBetweenStrings.length; i++) {
            if (differenceBetweenStrings[i] > 0)
                characters.add((char) (i + 'a'));
        }
        if (characters.isEmpty()) {
            return STRINGS_ARE_ANAGRAMS;
        }
        return characters.toString();
    }
}
