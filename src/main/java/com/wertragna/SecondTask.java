package com.wertragna;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SecondTask {

    private static final String COMMA_DELIMITER = ",";
    private static final String LINE = "----------------------------------------";

    public static void main(String[] args) {
        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("hello.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMITER);
                records.add(Arrays.asList(values));
            }
            System.out.println(records);
            printTable(records);

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }


    private static void printTable(List<List<String>> table) {
        List<Integer> maxLengths = findMaxLengths(table);
        System.out.println(maxLengths);
        String format = maxLengths.stream().map(Object::toString).reduce("", (a, b) -> a + "|%-" + b + "s") + "|\n";
        System.out.println(format);
        table.stream().findFirst().ifPresent(header -> System.out.format(format, header.toArray()));
        System.out.println(LINE);
        table.stream().skip(1).forEach(row -> System.out.format(format, row.toArray()));
        System.out.println(LINE);

    }

    private static List<Integer> findMaxLengths(List<List<String>> table) {
        List<Integer> maxLengths = new ArrayList<>();
        for (List<String> row : table) {
            int i = 0;
            for (String value : row) {
                if (maxLengths.size() < row.size()) {
                    maxLengths.add(value.length());
                } else {
                    if (maxLengths.get(i) < value.length()) {
                        maxLengths.set(i, value.length());
                    }
                }
                i++;
            }
        }
        return maxLengths;
    }

}
