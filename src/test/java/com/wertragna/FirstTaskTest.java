package com.wertragna;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class FirstTaskTest {

    @Test
    public void shouldReturnSuccessWhenVerifyAnagrams() {
        final String expected = "Strings are anagrams";
        String actual = FirstTask.verifyAnagrams("hello", "hello");
        assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnFailVerifyAnagramsIfDifferenceLength() {
        String expected = "Strings are not anagrams";
        String actual = FirstTask.verifyAnagrams("hellob", "hello");
        assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnCharactersVerifyAnagrams() {
        String expected = "[a, b]";
        String actual = FirstTask.verifyAnagrams("aabb", "bacd");
        assertEquals(expected, actual);
    }
}
